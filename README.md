## To use this project?

1. Fork/Clone
1. Install dependencies - `npm install`
1. Use to start the app - `npm start`
1. To build the app for production - `npm run build-prod`
1. To run tests use `npm run tdd`