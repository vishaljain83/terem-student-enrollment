import axios from 'axios';

export const FETCH_STUDENTS = "FETCH_STUDENTS";
export const FETCH_STUDENTS_DONE = "FETCH_STUDENTS_DONE";

function fetchStudentsStart() {
    return {
        type: FETCH_STUDENTS
    }
}

function fetchStudentsDone(payload) {
    return {
        type: FETCH_STUDENTS_DONE,
        payload: payload
    }
}

export function fetchStudents() {
    const url = 'https://s3.amazonaws.com/terem-student-api/students.json';
    
    // Thunk middleware knows how to handle functions.
    // It passes the dispatch method as an argument to the function,
    // thus making it able to dispatch actions itself.

    return function (dispatch) {
      // First dispatch: the app state is updated to inform
      // that the API call is starting.

      dispatch(fetchStudentsStart())

      // The function called by the thunk middleware can return a value,
      // that is passed on as the return value of the dispatch method.

      // In this case, we return a promise to wait for.
      // This is not required by thunk middleware, but it is convenient for us.

      return axios.get(url, {crossdomain: true})
        .then(
          response => {
            dispatch(fetchStudentsDone(response.data))
        })
        .catch(error =>
          // We can dispatch many times!
          // Here, we update the app state with the results of the API call.
          console.log(error)
        )
    }
}