export const ADD_CLASS = "ADD_CLASS";
export const GET_CLASSES = "GET_CLASSES";
export const VIEW_CLASS_DETAILS = "VIEW_CLASS_DETAILS";
export const DELETE_CLASS = "DELETE_CLASS";
export const ENROLL_STUDENT = "ENROLL_STUDENT";
export const UPDATE_CLASSES = "UPDATE_CLASSES";

export function addClass(payload) {
    return {
        type: ADD_CLASS,
        payload
    }  
}

export function getClasses() {
    return {
        type: GET_CLASSES
    }
}

export function enrollStudentToClass(payload) {    
    return {
        type: ENROLL_STUDENT,
        payload
    }
}

export function deleteClass(payload) {
    return {
        type: DELETE_CLASS,
        payload
    }
}

export function updateClasses(payload) {
    return {
        type: UPDATE_CLASSES,
        payload
    }
}