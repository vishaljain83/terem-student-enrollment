import { combineReducers } from 'redux'
import classReducer from './reducer_class';
import studentReducer from './reducer_student';

export default combineReducers ({
    classes: classReducer,
    students: studentReducer
})