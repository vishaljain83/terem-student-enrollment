import { FETCH_STUDENTS_DONE, ADD_STUDENT } from '../actions/student';

export default function (state = [], action){
    switch(action.type){
        
        case FETCH_STUDENTS_DONE:
            return [ ...state, action.payload.Students ];    
            
        
        case ADD_STUDENT:
            return [ ...state, action.payload ];    
    }

    return state;
}