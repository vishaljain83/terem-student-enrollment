import { 
    GET_CLASSES, 
    ADD_CLASS, 
    ENROLL_STUDENT, 
    DELETE_CLASS,
    UPDATE_CLASSES
} from '../actions/class';

let initialState = 
    [
        {
            classId: 'CS201',
            className: 'Software Architecture Design Patterns',
            startingDate: '2014-12-12',
            displayDate: '12/12/2014',
            enrolledStudents: 0,
            maxEnrolls: 15,
            students: []
        },
        {
            classId: 'CS105',
            className: 'Information Security',
            startingDate: '2014-12-12',
            displayDate: '12/12/2014',
            enrolledStudents: 0,
            maxEnrolls: 10,
            students: []
        },
        {
            classId: 'CS301',
            className: 'Mobile Applications',
            startingDate: '2014-12-12',
            displayDate: '12/12/2014',
            enrolledStudents: 0,
            maxEnrolls: 10,
            students: []
        }
    ]


export default function (state = initialState, action){

    switch(action.type){
        case GET_CLASSES:
            return state;
        
        case ADD_CLASS:
            return [ ...state, action.payload ]; 
            
        case ENROLL_STUDENT:
            return [ ...action.payload ]; 
            
        case DELETE_CLASS:
            return [ ...action.payload ];
            
        case UPDATE_CLASSES:
            return [ ...action.payload ];    
    }

    return state;
}