import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addClass } from '../actions/class';
import _ from 'lodash';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    appBar: {
      position: 'relative',
    },
    flex: {
      flex: 1,
    },
    container: {
        width: '100%',
        textAlign: 'center'
    },
    form:{
        marginTop: 0,
        marginRight: 'auto',
        marginLeft: 'auto',
        width: 250
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    },
    menu: {
        width: 200,
    },
});

class NewClass extends Component {

    constructor(props) {
        super(props);
       
        this.state = {
            classId: '',
            className: '',
            startingDate: '',
            displayDate: '',
            maxEnrolls: 0,
            enrolledStudents: 0,
            errorState: false
        }

        this.onClickHandler = this.onClickHandler.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.validData = this.validData.bind(this);
        this.getStartingDate = this.getStartingDate.bind(this);
    }

    validData() {
        const data = this.state;
        const fields = ['classId', 'className', 'maxEnrolls', 'startingDate']

        for (var i in fields) {
            let key = fields[i];
            if (data.hasOwnProperty(key)) {
               if(key === 'maxEnrolls' && data[key] <= 0)
                return false;

               if(String(data[key]).length === 0)
                return false; 
            }
        }
        return true;
    }

    onClickHandler(event) {
        event.preventDefault();

        if(!this.state.errorState && this.validData()) {
            this.props.addClass(this.state);
            this.props.close();
        }
    }
    
    getStartingDate() {
        const d = new Date();

        return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
    }

    handleChange(event) {

        if(event.target.id === 'classId'){
            let index = _.findIndex(this.props.data, ['classId', event.target.value]);

            this.setState({errorState: (index >= 0)});
        } else if(event.target.id === 'startingDate') {
            this.setState({displayDate: this.getStartingDate()});
        }

        this.setState({[event.target.id]: event.target.value});
    }

    render() {

        const { classes } = this.props;

        return (
            <div> 
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={this.props.close} aria-label="Close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            New Class
                        </Typography>
                        <Button 
                            color="inherit" 
                            onClick={this.onClickHandler}
                            label="Add Class">
                            Add Class
                        </Button>
                    </Toolbar>
                </AppBar>
                <div className={classes.container}>
                    <form className={classes.form} noValidate autoComplete="off">
                        <TextField
                            id="classId"
                            label="Class ID"
                            value={this.state.classId}
                            className={classes.textField}
                            margin="normal"
                            onChange={this.handleChange}
                            error={this.state.errorState}
                        />
                        <TextField
                            id="className"
                            label="Class Name"
                            value={this.state.className}
                            className={classes.textField}
                            margin="normal"
                            onChange={this.handleChange}
                        />
                        <TextField
                            id="maxEnrolls"
                            label="Maximum number of Enrolls"
                            value={this.state.maxEnrolls}
                            className={classes.textField}
                            type="number"
                            min="1"
                            margin="normal"
                            onChange={this.handleChange}
                        />
                        <TextField
                            id="startingDate"
                            label="Starting Date"
                            type="date"
                            value={this.state.startingDate}
                            className={classes.textField}
                            margin="normal"
                            onChange={this.handleChange}
                        />
                    </form>
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
  return ( bindActionCreators ({ addClass }, dispatch) );
}

const AddClass = connect(
  null,
  mapDispatchToProps
)(NewClass)

export default withStyles(styles)(AddClass);