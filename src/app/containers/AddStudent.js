import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchStudents } from '../actions/student';
import { enrollStudentToClass } from '../actions/class';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
    appBar: {
      position: 'relative',
    },
    flex: {
      flex: 1,
    },
    container: {
        width: '100%',
        textAlign: 'center'
    },
    form:{
        marginTop: 0,
        marginRight: 'auto',
        marginLeft: 'auto',
        width: 250
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
        textAlign: 'left'
    },
    menu: {
        width: 200,
    },
});

class AddStudent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            name: '',
            classId: '',
            className: ''
        }

        this.handleStudentChange = this.handleStudentChange.bind(this);
        this.handleClassChange = this.handleClassChange.bind(this);
        this.onClickHandler = this.onClickHandler.bind(this);
        this.findItem = this.findItem.bind(this);
        this.getEnrollmentDate = this.getEnrollmentDate.bind(this);
    }

    componentDidMount() {
        this.props.fetchStudents();
    }

    getEnrollmentDate() {
        const d = new Date();

        return `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
    }

    onClickHandler(event) {
        event.preventDefault();
        
        const item = this.findItem('classId', this.props.data, this.state.classId);
        const dateString = this.getEnrollmentDate();

        const student = {
            id: this.state.id,
            name: this.state.name,
            enrollment_date: dateString
        }

        item.students = [...item.students, student];
        item.enrolledStudents = item.students.length;
        
        this.props.enrollStudentToClass(this.props.data);
        this.props.close();
    }

    handleStudentChange(event) {
        const student = this.findItem('id', this.props.students[0], event.target.value);
        this.setState({
            id: student.id,
            name: student.name
        });
    }
    
    handleClassChange(event) {
        const item = this.findItem('classId', this.props.data, event.target.value);
        this.setState({
            classId: item.classId,
            className: item.className
        });
    }
    
    findItem(key, data, value) {
       return _.find(data, [key, value]);
    }

    render () {
        const { classes } = this.props;
        const students = this.props.students[0]
        
        return (
            <div> 
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit" onClick={this.props.close} aria-label="Close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Add Student
                        </Typography>
                        <Button 
                            color="inherit" 
                            onClick={this.onClickHandler}
                            label="Add Student">
                            Add Student
                        </Button>
                    </Toolbar>
                </AppBar>
                <div className={classes.container}>
                    <form className={classes.form} noValidate autoComplete="off">
                        <TextField
                            id="studentName"
                            select
                            label="Student Name"
                            className={classes.textField}
                            margin="normal"
                            helperText="Please select a Student"
                            value={this.state.id}
                            onChange={this.handleStudentChange}
                            SelectProps={{
                                MenuProps: {
                                  className: classes.menu,
                                },
                            }}
                        >
                        {students && students.map(student => (
                            <MenuItem key={student.id} value={student.id}>
                                {student.name}
                            </MenuItem>
                        ))}
                        </TextField>
                        <TextField
                            id="className"
                            select
                            label="Class Name"
                            className={classes.textField}
                            margin="normal"
                            value={this.state.classId}
                            onChange={this.handleClassChange}
                            helperText="Please select a Class"
                        >
                        {this.props.data.map(item => (
                            <MenuItem key={item.classId} value={item.classId}>
                                {item.className}
                            </MenuItem>
                        ))}
                        </TextField>
                    </form>
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return ( bindActionCreators ({ fetchStudents, enrollStudentToClass }, dispatch) );
}

function mapStateToProps({ students, classes }) {
    return { 
        students,
        data: classes 
    };
}

const AddStudentView = connect(
    mapStateToProps,
    mapDispatchToProps
)(AddStudent);

export default withStyles(styles)(AddStudentView);