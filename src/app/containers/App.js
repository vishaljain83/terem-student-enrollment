import React, { Component } from 'react';

import Classes from './Classes';

export default class App extends Component {
  render() {
    return (
      <div>
        <Classes id="classesView"/>
      </div>
    );
  }
}