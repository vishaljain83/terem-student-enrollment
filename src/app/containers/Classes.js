import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../withRoot';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getClasses, deleteClass, updateClasses } from '../actions/class';

import ClassCell from '../components/ClassCell';
import AddStudent from '../containers/AddStudent';
import NewClass from '../containers/NewClass';
import ClassItem from '../components/ClassItem';

import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

const styles = theme => ({
    root: {
      marginTop: theme.spacing.unit * 3,
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      overflowX: 'auto',
      textAlign: 'center',
      paddingLeft: 10,
      paddingTop: 10
    },
    button: {
        margin: theme.spacing.unit,
    },
    table: {
      minWidth: 700,
    },
    flex: {
        flex: 1,
    }
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class Classes extends Component {

    constructor(props) {
        super(props);

        this.state = {
            openNewClass: false,
            openAddStudent: false,
            openViewClass: false,
            openDeletePrompt: false,
            selectedClass: {},
            classIdToDelete: -1,
            studentIdToDelete: -1
        };

        this.handleClickOpenNewClass = this.handleClickOpenNewClass.bind(this);
        this.handleClickOpenNewStudent = this.handleClickOpenNewStudent.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.deleteClass = this.deleteClass.bind(this);
        this.viewClass = this.viewClass.bind(this);
        this.deleteStudent = this.deleteStudent.bind(this);
        this.deleteClassPrompt = this.deleteClassPrompt.bind(this);
        this.deleteStudentPrompt = this.deleteStudentPrompt.bind(this);
    }

    componentDidMount() {
        this.props.getClasses();
    }

    viewClass(classId) {
        const classData = _.filter(this.props.data, function(item) { return (item.classId === classId); });

        this.setState({ 
            openViewClass: true,
            selectedClass: classData[0] 
        });
    }

    deleteStudentPrompt(classId, studentId) {
        this.setState({ 
            openDeletePrompt: true, 
            classIdToDelete: classId,
            studentIdToDelete: studentId
        });
    }

    deleteStudent() {
        const classId = this.state.classIdToDelete;
        const studentId = this.state.studentIdToDelete;

        let classData = _.filter(this.props.data, function(item) { return (item.classId === classId); });
        let students = _.filter(classData[0].students, function(item) { return !(item.id === studentId); });
        classData[0].students = students;
        classData[0].enrolledStudents = students.length;

        this.props.updateClasses(this.props.data)
        this.handleClose();
    }

    deleteClassPrompt(classId) {
        this.setState({ 
            openDeletePrompt: true, 
            classIdToDelete: classId
        });
    }

    deleteClass() {
       const classId = this.state.classIdToDelete;
       const filteredItems = _.filter(this.props.data, function(item) { 
           return !(item.classId === classId); 
        });
       this.props.deleteClass(filteredItems);
       this.handleClose();
    }

    handleClickOpenNewClass() {
        this.setState({ openNewClass: true });
    }

    handleClickOpenNewStudent() {
        this.setState({ openAddStudent: true });
    }

    handleClose() {
        this.setState({ 
            openNewClass: false, 
            openAddStudent: false, 
            openViewClass: false,
            openDeletePrompt: false,
            selectedClass: {},
            classIdToDelete: -1,
            studentIdToDelete: -1
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <Paper className={classes.root}>
                <Typography variant="title" align="left" color="inherit" className={classes.flex}>
                    Classes
                </Typography>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Class ID</TableCell>
                            <TableCell>Class Name</TableCell>
                            <TableCell >Starting Date</TableCell>
                            <TableCell numeric># of Students</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.data.map(item=> {
                            return(
                                <ClassCell 
                                    key={item.classId} 
                                    data={item} 
                                    view={this.viewClass} 
                                    delete={this.deleteClassPrompt}/>
                            )
                        })}        
                    </TableBody>
                </Table>
                <Button 
                    onClick={this.handleClickOpenNewClass} 
                    variant="contained" 
                    color="secondary" 
                    className={classes.button}>
                    New Class
                </Button>
                <Dialog
                    fullScreen
                    open={this.state.openNewClass}
                    onClose={this.handleClose}
                    TransitionComponent={Transition}>
                    <NewClass data={this.props.data} close={this.handleClose}/>
                </Dialog>
                <Button 
                    onClick={this.handleClickOpenNewStudent} 
                    variant="contained" 
                    color="secondary" 
                    className={classes.button}>
                    Add Student
                </Button>
                <Dialog
                    fullScreen
                    open={this.state.openAddStudent}
                    onClose={this.handleClose}
                    TransitionComponent={Transition}>
                    <AddStudent close={this.handleClose}/>
                </Dialog>

                <Dialog
                    fullScreen
                    open={this.state.openViewClass}
                    onClose={this.handleClose}
                    TransitionComponent={Transition}>
                    <ClassItem 
                        close={this.handleClose} 
                        data={this.state.selectedClass} 
                        delete={this.deleteStudentPrompt}/>
                </Dialog>
                <Dialog
                    disableBackdropClick
                    disableEscapeKeyDown
                    open={this.state.openDeletePrompt}
                    onClose={this.handleClose}>

                    <DialogTitle>Warning!</DialogTitle>
                    <DialogContent>
                        <Typography variant="caption" align="left" color="inherit" className={classes.flex}>
                            Are you sure you want to delete this?
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={
                            (this.state.studentIdToDelete !== -1) ? this.deleteStudent: this.deleteClass
                            } color="primary">
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>    
            </Paper>  
        ) 
    }
}

function mapDispatchToProps(dispatch){
    return ( bindActionCreators ({ getClasses, deleteClass, updateClasses}, dispatch) );
}

function mapStateToProps({ classes }) {
    return { data: classes };
}

const ClassView = connect(
    mapStateToProps,
    mapDispatchToProps
)(Classes);

export default withRoot(withStyles(styles)(ClassView));