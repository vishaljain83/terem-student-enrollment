import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell'; 
import DeleteIcon from '@material-ui/icons/Delete';

const styles = theme => ({
    appBar: {
      position: 'relative',
    },
    flex: {
      flex: 1,
    }
});

const ClassItem = (props) => {
    const { classes } = props;
    
    return (
       <div> 
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton color="inherit" onClick={props.close} aria-label="Close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.flex}>
                        {props.data.className}
                    </Typography>
                </Toolbar>
            </AppBar>

            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Student ID</TableCell>
                        <TableCell>Student Name</TableCell>
                        <TableCell>Enrollment Date</TableCell>
                        <TableCell>Actions</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.data.students && props.data.students.map(student=> {
                        return(
                            <TableRow key={student.id}>
                                <TableCell>{student.id}</TableCell>
                                <TableCell>{student.name}</TableCell> 
                                <TableCell>{student.enrollment_date}</TableCell>
                                <TableCell>
                                    <IconButton 
                                        color="primary" 
                                        className={classes.button} 
                                        aria-label="Delete" 
                                        onClick={ () => { 
                                            props.delete(props.data.classId, student.id) 
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        )
                    })}     
                </TableBody>
            </Table>        
        </div>
    )
}

export default withStyles(styles)(ClassItem);