import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Description from '@material-ui/icons/Description'

const styles = theme => ({
    button: {
      margin: theme.spacing.unit - 15
    }
});

const ClassCell = (props) => {

    const { classes } = props;
    
    return (
        <TableRow key={props.data.classId}>
            <TableCell>{props.data.classId}</TableCell>
            <TableCell>{props.data.className}</TableCell> 
            <TableCell>{props.data.displayDate}</TableCell>
            <TableCell numeric>{props.data.enrolledStudents}</TableCell>
            <TableCell>
                <IconButton 
                    color="primary" 
                    className={classes.button} 
                    aria-label="View" 
                    onClick={ () => { props.view(props.data.classId) }}
                >
                    <Description />
                </IconButton>
                <IconButton 
                    color="primary" 
                    className={classes.button} 
                    aria-label="Delete" 
                    onClick={ () => { props.delete(props.data.classId) }}
                >
                    <DeleteIcon />
                </IconButton>
            </TableCell>
        </TableRow>
    )
}

export default withStyles(styles)(ClassCell);

