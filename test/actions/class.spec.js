import * as actions from '../../src/app/actions/class';

describe('class actions', () => {
  it('should create an action to add a Class', () => {
    const payload = {
        classId: 'CS201',
        className: 'Software Architecture Design Patterns',
        startingDate: '2014-12-12',
        displayDate: '12/12/2014',
        enrolledStudents: 0,
        maxEnrolls: 15,
        students: []
    }

    const expectedAction = {
      type: actions.ADD_CLASS,
      payload
    }
    expect(actions.addClass(payload)).to.deep.equal(expectedAction)
  })

  it('should create an action to get a Classes', () => {

    const expectedAction = {
      type: actions.GET_CLASSES,
    }
    expect(actions.getClasses()).to.deep.equal(expectedAction)
  })

  it('should create an action to delete a Class', () => {
    const payload = {}

    const expectedAction = {
      type: actions.DELETE_CLASS,
      payload
    }
    expect(actions.deleteClass(payload)).to.deep.equal(expectedAction)
  })

  it('should create an action to enroll a student to a class', () => {
    const payload = {
        classId: 'CS201',
        className: 'Software Architecture Design Patterns',
        startingDate: '2014-12-12',
        displayDate: '12/12/2014',
        enrolledStudents: 0,
        maxEnrolls: 15,
        students: [
            {
                id: 'P100',
                name: 'John Snow',
                enrollment_date: '12/2/2014'
            }
        ]
    }

    const expectedAction = {
      type: actions.ENROLL_STUDENT,
      payload
    }
    expect(actions.enrollStudentToClass(payload)).to.deep.equal(expectedAction)
  })

  it('should create an action to update a class', () => {
    const payload = {
        classId: 'CS201',
        className: 'Software Architecture Design Patterns',
        startingDate: '2014-12-12',
        displayDate: '12/12/2014',
        enrolledStudents: 0,
        maxEnrolls: 15,
        students: []
    }

    const expectedAction = {
      type: actions.UPDATE_CLASSES,
      payload
    }
    expect(actions.updateClasses(payload)).to.deep.equal(expectedAction)
  })
})