import React from 'react';
import ClassItem from '../../src/app/components/ClassItem';

describe('ClassItem' , () => {

    it('Check to see if the ClassItem is rendered', () => {
        const wrapper = shallow(<ClassItem />);

        it('renders something', () => {
            expect(wrapper).to.exist;
        });
    });
});