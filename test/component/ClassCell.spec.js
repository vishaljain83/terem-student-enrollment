import React from 'react';
import ClassCell from '../../src/app/components/ClassCell';

describe('ClassCell' , () => {

    it('Check to see if the ClassCell is rendered', () => {
        const wrapper = shallow(<ClassCell />);

        it('renders something', () => {
            expect(wrapper).to.exist;
        });
    });
});