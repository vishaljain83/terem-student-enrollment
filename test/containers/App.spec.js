import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../../src/app/configureStore';
import App from '../../src/app/containers/App';

describe('App' , () => {

    it('Check to see if the App is rendered', () => {
        const wrapper = shallow(<App />);

        it('renders something', () => {
            expect(wrapper).to.exist;
        });
    });

    it('Classes is rendered', () => {

        const store = configureStore();

        const wrapper = mount(    
            <Provider store={store}>
                <App />
            </Provider>      
        );

        const classesView = wrapper.find('classesView');

        expect(classesView).to.exist;
    });
});